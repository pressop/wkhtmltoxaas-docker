FROM debian:stretch

ARG DEB_URL
ENV DEB_URL ${DEB_URL:-'https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb'}

RUN apt-get update && apt-get upgrade -y && apt-get install -y curl apt-transport-https ca-certificates fontconfig \
    libfontenc1 libjpeg62-turbo libxext6 libxfont1 libxrender1 x11-common xfonts-75dpi xfonts-base xfonts-encodings \
    xfonts-utils gnupg supervisor

RUN curl -L $DEB_URL -o wkhtmltox.deb
RUN dpkg -i wkhtmltox.deb && \
    rm -f wkhtmltox.deb

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs && \
    curl -o- -L https://yarnpkg.com/install.sh | bash

WORKDIR /opt

RUN /root/.yarn/bin/yarn add wkhtmltoxaas

COPY supervisord.conf /etc/supervisord.conf
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
